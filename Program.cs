﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace ParagraphAnalyzer
{
    class Program
    {
        /// <summary>
        /// The main function of the program. All console output and interaction occurs
        /// within this function.
        /// </summary>
        static void Main(string[] args)
        {
            if (args.Length == 0) {
                Console.WriteLine("You must enter a short paragraph.");
                return;
            }

            string paragraph = args[0];

            // string paragraph = "The lone lamp post of the town flickered, not quite dead but definitely on its way out. ";
            // paragraph += "Suitcase by her side, she paid no heed to the bib, the street or the town. ";
            // paragraph += "Was it a car or a cat I saw? ";
            // paragraph += "A racecar was coming down the street and with her rotor outstretched and thumb in the air, she had a plan. ";
            // paragraph += "Murder for a jar of red rum.";

            // Give the number of palindrome words.
            int wordCount = FindPalindromeWords(paragraph);
            Console.WriteLine($"The paragraph contains {wordCount} palindrome word(s).");

            // Give the number of palindrome sentences.
            int sentCount = FindPalindromeSents(paragraph);
            Console.WriteLine($"The paragraph contains {sentCount} palindrome sentence(s).");

            // List the unique words of a paragraph with the count of the word instance.
            Dictionary<string, int> dict = FindUniqueWords(paragraph);
            Console.WriteLine("Found the following unique words in the paragraph:");
            foreach (string key in dict.Keys)
            {
                Console.WriteLine($"'{key}' was found " + dict[key].ToString() + " times(s).");
            }

            string msg = "Please enter a single letter: ";
            Console.Write(msg);
            
            string input = Console.ReadLine();
            if (input.Length != 1 || !(Char.IsLetter(char.Parse(input))))
            {
                Console.WriteLine("You didn't enter a single letter. Please try again.");
            }
            else
            {
                Console.WriteLine($"You entered the letter: {input}");                

                // List all words containing the input letter.
                List<string> words = FindWordsWithLetter(dict.Keys.ToList(), input[0]);
                Console.WriteLine($"Found the following words containing the letter '{input}' in the paragraph:");
                foreach (string word in words)
                {
                    Console.WriteLine(word);
                }
            }
        }


        /// <summary>
        /// Finds words that are palindromes from a paragraph. Uses another method to
        /// split the paragraph into words and then each word is checked against the
        /// reverse of itself.
        /// </summary>
        static int FindPalindromeWords(string paragraph)
        {
            int count = 0;

            string[] stripped_words = GetWordsFromParagraph(paragraph);

            foreach (string word in stripped_words)
            {
                string lowered = word.ToLower();
                if (lowered.SequenceEqual(lowered.Reverse())) {
                    count++;
                }
            }

            return count;
        }


        /// <summary>
        /// Finds sentences that are palindromes from a paragraph. The paragrpah
        /// is split by punctuation and whitespace, then all non-word characters
        /// are stripped to create a string which is checked if it is equal to
        /// the reverse of itself.
        /// </summary>
        static int FindPalindromeSents(string paragraph)
        {
            int count = 0;

            string[] sentences = Regex.Split(paragraph, @"[.!?]\s+(?=[A-Z])");

            // Strip non-word characters.
            foreach (string sentence in sentences)
            {
                string replaced = Regex.Replace(sentence, @"[^\w]", "").ToLower();
                if (replaced.SequenceEqual(replaced.Reverse())) {
                    count++;
                }
            }

            return count;
        }


        /// <summary>
        /// Finds words that are unique in a paragraph by tracking the word
        /// and the number of times it appears in a dictionary.
        /// </summary>
        static Dictionary<string, int> FindUniqueWords(string paragraph)
        {
            Dictionary<string, int> dict = new Dictionary<string, int>();

            string[] stripped_words = GetWordsFromParagraph(paragraph);
            foreach (string word in stripped_words)
            {
                if (dict.ContainsKey(word))
                {
                    dict[word]++;
                }
                else
                {
                    dict[word] = 1;
                }
            }

            return dict;
        }


        /// <summary>
        /// Finds words from a list of words that contain a specified letter.
        /// </summary>
        static List<string> FindWordsWithLetter(List<string> words, char letter)
        {
            List<string> foundWords = new List<string>();

            foreach (string word in words)
            {
                if (word.Contains(letter))
                {
                    foundWords.Add(word);
                }
            }            

            return foundWords;
        }


        /// <summary>
        /// Gets words from a paragraph string by stripping all non-alpha characters and 
        /// splitting the string by whitespace.
        /// </summary>
        static string[] GetWordsFromParagraph(string paragraph)
        {
            string replaced = Regex.Replace(paragraph, @"[^\w\s]|\d+", "").ToLower();
            string[] words = Regex.Split(replaced, @"\s+");

            return words;
        }
    }
}
