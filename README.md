# Paragraph Analyzer

This repo contains the source code for a simple C# console application that does several things. It analyzes an input paragraph string and \(1\) finds the number of palindrome words, \(2\) finds the number of palindrome sentences, \(3\) lists the unique words in the paragraph and keeps count of each word instance, and \(4\) lists all words containing an input letter.

The `Program.cs` file contains documentation comments for each method explaining what each method does and briefly how it does it. For convenience, these are listed below and are explained in a little more detail.


**Main()**<br>
The main function of the program. All console output and interaction occurs within this function.

**FindPalindromeWords()**<br>
Finds words that are palindromes from a paragraph. Uses another method to split the paragraph into words and then each word is checked against the reverse of itself using the LINQ method `SequenceEqual`. Returns the count of words found that are valid palindromes.

**FindPalindromeSents()**<br>
Finds sentences that are palindromes from a paragraph. The paragraph is split by punctuation and whitespace, then all non-word characters are stripped to create a string which is checked if it is equal to the reverse of itself using the LINQ method `SequenceEqual`. Returns the count of sentences found that are valid palindromes.

**FindUniqueWords()**<br>
Finds words that are unique in a paragraph by tracking the word and the number of times it appears in a dictionary. The dictionary's keys are the words and the values are the number of times that particular word appears in the paragraph. Uses the `GetWordsFromParagraph` function to retrieve the paragraph as a list of words, which is then checked for uniqueness. Returns the aformentioned dictionary.

**FindWordsWithLetter()**<br>
Finds words from a list of words that contain a specified letter.

**GetWordsFromParagraph()**<br>
Gets words from a paragraph string by stripping all non-alpha characters and splitting the string by whitespace. Uses regex methods to replace all non-word, non-whitespace, digit characters in the paragraph and then splits the result by whitespace.
